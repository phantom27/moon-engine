const Path = require('fire-path')
const GZip = require('../cocos2d/compression/gzip')

let DungeonMapAsset = cc.Class({
    name: 'dungeon.DungeonMapAsset',
    extends: cc.Asset,

    properties: {
        dmRawDataBinary: null,
        dmInfo: null,
        row: 0,
        col: 0,
        tiledWidth: 0,
        tiledHeight: 0,
        mapWidth: 0,
        mapHeight: 0,

        /**
         * @property {Texture2D} backgroundTexture
         */
        backgroundTexture: {
            default: null,
            type: cc.Texture2D
        },

        /**
         * @property {Texture2D[]} imageTextures
         */
        imageTextures: {
            default: [],
            type: [cc.Texture2D]
        },

        imageTextureIndex: {
            default: {},
            type: Object
        },

        /**
         * @property {Texture2D[]} spriteTextures
         */
        spriteTextures: {
            default: [],
            type: [cc.Texture2D]
        },

        spriteTextureIndex: {
            default: {},
            type: Object
        },
    },
    
    statics: {
        preventDeferredLoadDependents: true
    },

    createNode: CC_EDITOR && function (callback) {
        let node = new cc.Node(this.name);
        let dungeonMap = node.addComponent(dungeon.DungeonMap);
        dungeonMap.dmAsset = this;
        return callback(null, node);
    },

    getDmInfo() {
        if (this.dmInfo == null) {
            if (this.dmRawDataBinary != null) {
                try {
                    let dmJsonStr = GZip.gunzip(this.dmRawDataBinary);
                    this.dmInfo = JSON.parse(dmJsonStr);
                } catch (e) {
                    this.dmInfo = {};
                    cc.error(e);
                }
            }
            else{
                this.dmInfo = {};
            }
        }
        return this.dmInfo;
    },

    allSpriteListInfo() {
        return this.getDmInfo()['depend_sprite_info'];
    },

    allAnimationTileInfo() {
        let infos = [];
        let tileSpriteInfos = this.getDmInfo()['tile_sprite_infos'];
        let animations = this.getDmInfo()['animations'];
        let spriteListInfos = this.getDmInfo()['depend_sprite_info'];

        for (let tileSpriteInfoIndex in tileSpriteInfos) {
            let tileSpriteInfo = tileSpriteInfos[tileSpriteInfoIndex];
            if (tileSpriteInfo['type'] == 'animation') {
                let frames = [];
                let index = tileSpriteInfoIndex.split('#');
                let animationName = tileSpriteInfo['info']['animation_name'];
                let animationInfo = animations[animationName];
                let animationRate = animationInfo['rate'];
                let animationFrames = animationInfo['key_frames'];

                for (let frame of animationFrames) {
                    let texture = null;
                    let uv = {};
                    let spriteFrame = null;
                    if (frame['type'] == 'image') {
                        texture = this.imageTextures[this.imageTextureIndex[frame['filename']]];
                        uv = {
                            'left': 0,
                            'top': 0,
                            'right': 1,
                            'bottom': 1
                        }
                    } else if (frame['type'] == 'sprite') {
                        let spriteInfo = spriteListInfos[frame['sprite_name']];
                        let textureWidth = spriteInfo['texture_width'];
                        let textureHeight = spriteInfo['texture_height'];
                        let frameInfo = spriteInfo['frames'][frame['frame_name']];
                        let left = frameInfo['left'] / textureWidth;
                        let top = frameInfo['top'] / textureHeight;
                        let width = frameInfo['width'] / textureWidth;
                        let height = frameInfo['height'] / textureHeight;

                        texture = this.spriteTextures[this.spriteTextureIndex[frame['sprite_name']]];
                        uv = {
                            'left': left,
                            'top': top,
                            'right': left + width,
                            'bottom': top + height
                        }
                        // spriteFrame = texture.getSpriteFrame(Path.basenameNoExt(frame['frame_name']));
                    }

                    frames.push({
                        'texture': texture,
                        'uv': uv,
                        'spriteFrame': spriteFrame
                    })
                }

                infos.push({
                    'row': index[0],
                    'col': index[1],
                    'animation_name': animationName,
                    'animation_rate': animationRate,
                    'frames': frames
                })
            }
        }

        return infos;
    },

    allFreeTileNodes() {
        return this.getDmInfo()['free_tile_nodes'];
    },

    //
    // only for Editor
    //

    // 获取图片资源对应SpriteFrame的uuid
    queryImageSpriteFrameUuid: CC_EDITOR && function (filename, callback) {
        if (!(filename in this.imageTextureIndex)) {
            return;
        }

        let imageTextureIndex = this.imageTextureIndex[filename];
        let texture2d = this.imageTextures[imageTextureIndex];

        Editor.assetdb.queryUrlByUuid(texture2d._uuid, function (e1, url) {
            if (e1) {
                cc.error(e1.toString());
                return;
            }

            let spriteFrameUrl = url + '/' + filename.substring(0, filename.indexOf('.'));
            Editor.assetdb.queryUuidByUrl(spriteFrameUrl, function (e2, uuid) {
                if (e2) {
                    cc.error(e2.toString());
                    return;
                }

                if (callback) {
                    callback(uuid);
                }
            });
        });
    },

    // 获取精灵图内某帧的uuid
    querySpriteFrameUuid: CC_EDITOR && function (spriteName, frameName, callback) {
        if (!(spriteName in this.spriteTextureIndex)) {
            return;
        }

        let spriteAtlas = this.spriteTextures[this.spriteTextureIndex[spriteName]];
        let uuid = spriteAtlas._spriteFrames[frameName.substring(0, frameName.indexOf('.'))]._uuid;

        if (uuid !== undefined && callback) {
            callback(uuid);
        }
    },

    // 获取图片资源对应uuid
    queryPictureSpriteFrameUuid: CC_EDITOR && function (freeTilePictureInfo, callback) {
        if (!('type' in freeTilePictureInfo)) {
            return;
        }

        if (freeTilePictureInfo['type'] == 'image') {
            this.queryImageSpriteFrameUuid(freeTilePictureInfo['filename'], callback);
        } else if (freeTilePictureInfo['type'] == 'sprite') {
            this.querySpriteFrameUuid(freeTilePictureInfo['sprite_name'], freeTilePictureInfo['frame_name'], callback);
        }
    },

    // 获取动画第一帧图像资源uuid
    queryAnimationFirstSpriteFrameUuid: CC_EDITOR && function (animationName, callback) {
        if (!(animationName in this.getDmInfo()['animations'])) {
            return;
        }

        let animationInfo = this.getDmInfo()['animations'][animationName];
        let keyFrames = animationInfo['key_frames'];

        if (keyFrames.length <= 0) {
            return;
        }

        this.queryPictureSpriteFrameUuid(keyFrames[0], function (uuid) {
            if (callback) {
                callback(uuid);
            }
        })
    },

    // 获取动画所有帧图像资源uuid
    queryAnimationSpriteFramesUuidList: CC_EDITOR && function (animationName, callback) {
        if (!(animationName in this.getDmInfo()['animations'])) {
            return;
        }

        let animationInfo = this.getDmInfo()['animations'][animationName];
        let keyFrames = animationInfo['key_frames'];
        let totalFrames = keyFrames.length;
        let recordedFrameCount = 0;
        let uuidList = [];

        for (let frameInfoIndex = 0; frameInfoIndex < totalFrames; frameInfoIndex++) {
            this.queryPictureSpriteFrameUuid(keyFrames[frameInfoIndex], function (uuid) {
                uuidList[frameInfoIndex] = uuid;
                if (++recordedFrameCount >= totalFrames && callback) {
                    callback(animationInfo['rate'], uuidList);
                }
            });
        }
    },

    // 获取指定AnimationClip的uuid
    queryAnimationClipUuid: CC_EDITOR && function (animationName, callback) {
        this.queryAnimationAssetFolderUrl(function (animationFolderPath) {
            let animationClipPath = animationFolderPath + '/' + animationName + '.anim';
            Editor.assetdb.queryUuidByUrl(animationClipPath, function (err, uuid) {
                if (err) {
                    cc.error(err.toString());
                    return;
                }

                if (callback) {
                    callback(uuid);
                }
            });
        })
    },

    // 获取Animation资源根目录url
    queryAnimationAssetFolderUrl: CC_EDITOR && function (callback) {
        Editor.assetdb.queryUrlByUuid(this._uuid, function (err, url) {
            if (err) {
                cc.error(err.toString());
                return;
            }

            let animationFolderPath = cc.path.dirname(url) + '/resources/animations';
            if (callback) {
                callback(animationFolderPath);
            }
        });
    },

    // 确保Animation资源根目录存在
    makeSureAnimationAssetFolderExist: CC_EDITOR && function (callback = null) {
        this.queryAnimationAssetFolderUrl(function (animationFolderPath) {
            Editor.assetdb.queryUuidByUrl(animationFolderPath, function (err, uuid) {
                if (uuid === undefined) {
                    Editor.assetdb.create(animationFolderPath, null, function (create_err, infos) {
                        if (create_err) {
                            cc.error(create_err.toString());
                        } else if (callback) {
                            callback(animationFolderPath);
                        }
                    })
                } else if (callback) {
                    callback(animationFolderPath);
                }
            });
        });
    },

    // 删除Animation资源根目录
    removeAnimationAssetFolder: CC_EDITOR && function() {
        this.queryAnimationAssetFolderUrl(function (animationFolderPath) {
            Editor.assetdb.queryUuidByUrl(animationFolderPath, function (err, uuid) {
                if (uuid !== undefined) {
                    Editor.assetdb.delete(animationFolderPath);
                }
            });
        });
    },

    // 生成AnimationClip资源对象
    generateAnimationClipAssetObject: CC_EDITOR && function (animationInfo, animationUuidList) {
        let interval = animationInfo['rate'] / 1000;
        let currentFrame = 0;
        let spriteFrameAnimation = [];

        for (let frameUuid of animationUuidList) {
            spriteFrameAnimation.push({
                'frame': currentFrame,
                'value': {
                    '__uuid__': frameUuid
                }
            });
            currentFrame += interval;
        }


        let animObj = {};
        animObj['__type__'] = 'cc.AnimationClip';
        animObj['_name'] = animationInfo['animation_name'];
        animObj['_objFlags'] = 0;
        animObj['_native'] = '';
        animObj['_duration'] = animationUuidList.length * interval;
        animObj['sample'] = 60;
        animObj['speed'] = 1;
        animObj['wrapMode'] = 2;
        animObj['curveData'] = {
            'comps': {
                'cc.Sprite': {
                    'spriteFrame': spriteFrameAnimation
                }
            }
        };
        animObj['events'] = [];

        return animObj;
    },

    // 生成AnimationClip实体
    generateAnimationClipAsset: CC_EDITOR && function (animationInfo, animationAssetPath) {
        // 获取动画所有帧的uuid
        this.queryAnimationSpriteFramesUuidList(animationInfo['animation_name'], (animationRate, animationUuidList) => {
            // 生成anim对象
            let animObj = this.generateAnimationClipAssetObject(animationInfo, animationUuidList);

            // 保存Animation Clip
            Editor.assetdb.create(animationAssetPath, JSON.stringify(animObj), function(err, infos) {
                if (err) {
                    cc.error(err.toString());
                }
            });
        });
    },

    // 生成所有AnimationClip
    generateAnimationResources: CC_EDITOR && function () {
        this.makeSureAnimationAssetFolderExist((animationFolderPath) => {
            let animations = [];

            for (let freeTile of this.allFreeTileNodes()) {
                if (freeTile['type'] != 'animation') {
                    continue;
                }

                let animationName = freeTile['info']['animation']['animation_name'];
                if (animations.indexOf(animationName) != -1) {
                    continue;
                }
                animations.push(animationName);

                //
                // 为动画生成对应的AnimationClip资源
                //

                // 拼接AnimationClip路径
                let animationAssetPath = animationFolderPath + '/' + animationName + '.anim';

                // 如果存在同名AnimationClip则不生成
                Editor.assetdb.queryUuidByUrl(animationAssetPath, (err, uuid) => {
                    if (uuid != undefined) {
                        return;
                    }

                    this.generateAnimationClipAsset(freeTile['info']['animation'], animationAssetPath);
                })
            }
        });
    }
});

dungeon.DungeonMapAsset = DungeonMapAsset;
module.exports = DungeonMapAsset;