// LICENSE HZQ
//
// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

'use strict';

require('./DungeonMapRenderLayer');
require('./FreeTileSensor');

let DungeonMap = cc.Class({
    name: 'dungeon.DungeonMap',
    extends: cc.Component,

    editor: CC_EDITOR && {
        executeInEditMode: true,
        menu: 'dungeon/DungeonMap',
        inspector: 'unpack://engine/dungeonmap/inspector/inspector.js',
    },

    properties: {
        _dmFile: {
            default: null,
            type: dungeon.DungeonMapAsset
        },
        dmAsset : {
            get () {
                return this._dmFile;
            },
            set (value, force) {
                if (this._dmFile !== value || (CC_EDITOR && force)) {
                    this._dmFile = value;
                    this._applyLayout();
                }

                if (!this._dmFile) {
                    this._clearLayout();
                }
            },
            type: dungeon.DungeonMapAsset
        },
        freeTileContainer: {
            default: null,
            type: cc.Node
        },
        sceneColliderHandlers: {
            default: [],
            type: cc.Component.EventHandler
        }
    },

    dungeonName() {
        if (this.dmAsset == null) {
            return "";
        }

        return this.dmAsset.getDmInfo()['dungeon_name'];
    },

    rootFreeTileNodeContainerNode() {
        return this.freeTileContainer ? this.freeTileContainer : this.node;
    },

    generateFreeTileContainerNodeName() {
        let dungeonName = this.dungeonName();
        if (dungeonName == "") {
            return "";
        }

        return "FreeTileNodeContainer[" + dungeonName + "]";
    },

    mapWidth() {
        if (this.dmAsset) {
            return this.dmAsset.mapWidth;
        }
        return 0;
    },

    mapHeight() {
        if (this.dmAsset) {
            return this.dmAsset.mapHeight;
        }
        return 0;
    },

    tiledWidth() {
        if (this.dmAsset) {
            return this.dmAsset.tiledWidth;
        }
        return 0;
    },

    tiledHeight() {
        if (this.dmAsset) {
            return this.dmAsset.tiledHeight;
        }
        return 0;
    },
    
    row() {
        if (this.dmAsset) {
            return this.dmAsset.row;
        }
        return 0;
    },

    col() {
        if (this.dmAsset) {
            return this.dmAsset.col;
        }
        return 0;
    },

    tiles() {
        if ('tiles' in this.dmInfo) {
            return this.dmInfo['tiles'];
        }
        return [];
    },

    tileExtraInfos() {
        if ('tile_extra_infos' in this.dmInfo) {
            return this.dmInfo['tile_extra_infos'];
        }
        return {};
    },

    indexToPosition(index) {
        let result = index.split('#');
        if (result.length != 2) {
            return cc.v2(0, 0);
        }
        return cc.v2(parseInt(result[1]), parseInt(result[0]));
    },

    startPoints() {
        let results = [];
        let extraInfos = this.tileExtraInfos();
        for (let index in extraInfos) {
            if (extraInfos[index] != 'StartPoint') {
                continue;
            }

            let pos = this.indexToPosition(index);
            results.push(this.convertToWorldPosition(pos.y, pos.x));
        }

        return results;
    },

    ctor() {
        this.dmInfo = {};
        this.renderLayer = null;
        this.colliderInfo = [];
        this.sceneAreasInfo = {};
        this.sceneAreas = {};
    },

    __preload() {
        if (this.dmAsset) {
            try {
                this.dmInfo = this.dmAsset.getDmInfo();
                this.colliderInfo = this.dmInfo['collider_info'];
                this.sceneAreasInfo = this.dmInfo['scene_areas'];

                let tagIndex = 0;
                for (let areaName in this.sceneAreasInfo) {
                    this.sceneAreas[tagIndex++] = areaName;
                }
            } catch {

            }
        }
    },

    onLoad() {
        this.renderLayer = this.node.getComponent(dungeon.DungeonMapRenderLayer);

        if (!CC_EDITOR) {
            // 布置碰撞区域
            this._applyColliderArea();

            // 布置感应区域
            this._applySceneArea();
        }
    },

    onEnable() {
        this.registerEvents('on');
    },

    onDisable() {
        this.registerEvents('off');
    },

    onCollisionEnter(other, self) {
        if (!this.sceneColliderHandlers.length) {
            return;
        }

        let tagIndex = self.tag;
        if (!(tagIndex in this.sceneAreas)) {
            return;
        }

        cc.Component.EventHandler.emitEvents(this.sceneColliderHandlers, {
            'areaName': this.sceneAreas[tagIndex],
            'colliderEvent': 'onCollisionEnter',
            'other': other,
            'self': self
        });
    },

    onCollisionStay(other, self) {
        if (!this.sceneColliderHandlers.length) {
            return;
        }

        let tagIndex = self.tag;
        if (!(tagIndex in this.sceneAreas)) {
            return;
        }

        cc.Component.EventHandler.emitEvents(this.sceneColliderHandlers, {
            'areaName': this.sceneAreas[tagIndex],
            'colliderEvent': 'onCollisionStay',
            'other': other,
            'self': self
        });
    },

    onCollisionExit(other, self) {
        if (!this.sceneColliderHandlers.length) {
            return;
        }

        let tagIndex = self.tag;
        if (!(tagIndex in this.sceneAreas)) {
            return;
        }

        cc.Component.EventHandler.emitEvents(this.sceneColliderHandlers, {
            'areaName': this.sceneAreas[tagIndex],
            'colliderEvent': 'onCollisionExit',
            'other': other,
            'self': self
        });
    },

    sharpChanged() {
        if (this.renderLayer) {
            this.renderLayer.dirty();
        }

        this._updateColliderArea();
        this._updateSceneArea();
        this._updateFreeTileContainer();
    },

    registerEvents(op = 'on') {
        if (op != 'on' && op != 'off') {
            return false;
        }

        this.node[op](cc.Node.EventType.SIZE_CHANGED, this.sharpChanged, this);
        this.node[op](cc.Node.EventType.ANCHOR_CHANGED, this.sharpChanged, this);
        this.node[op](cc.Node.EventType.POSITION_CHANGED, this.sharpChanged, this);
        this.node[op](cc.Node.EventType.SCALE_CHANGED, this.sharpChanged, this);
        this.node[op](cc.Node.EventType.ROTATION_CHANGED, this.sharpChanged, this);
        this.node[op](cc.Node.EventType.COLOR_CHANGED, this.sharpChanged, this);
        this.node[op](cc.Node.EventType.SIBLING_ORDER_CHANGED, this.sharpChanged, this);
        return true;
    },

    _updateColliderArea() {
        if (this.colliderInfo.length <= 0 || !this.dmAsset) {
            return;
        }

        let hroiFactor = this.node.width / this.dmAsset.mapWidth;
        let vertFactor = this.node.height / this.dmAsset.mapHeight;
        let chainColliders = this.node.getComponents(cc.PhysicsChainCollider);

        if (chainColliders.length <= 0) {
            return;
        }

        for (let index in chainColliders) {
            let chainCollider = chainColliders[index];
            let areaIndex = chainCollider.tag;

            if (areaIndex < 0 || areaIndex >= chainColliders.length) {
                continue;
            }

            let area = this.colliderInfo[areaIndex];
            chainCollider.points = [];
            for (let point of area) {
                chainCollider.points.push(cc.v2(point.x * hroiFactor, -point.y * vertFactor))
            }

            chainCollider.apply();
        }
    },

    _updateSceneArea() {
        if (!this.dmAsset) {
            return;
        }

        let hroiFactor = this.node.width / this.dmAsset.mapWidth;
        let vertFactor = this.node.height / this.dmAsset.mapHeight;
        let sceneAreaColliders = this.node.getComponents(cc.Collider);

        if (sceneAreaColliders.length <= 0) {
            return;
        }

        for (let index in sceneAreaColliders) {
            let sceneCollider = sceneAreaColliders[index];
            let sceneAreaIndex = sceneCollider.tag;
            
            if (sceneAreaIndex < 0 || !(sceneAreaIndex in this.sceneAreas)) {
                continue;
            }

            let sceneAreaName = this.sceneAreas[sceneAreaIndex];
            let sceneAreaInfo = this.sceneAreasInfo[sceneAreaName];

            let left = sceneAreaInfo['rect']['left'];
            let top = sceneAreaInfo['rect']['top'];
            let width = sceneAreaInfo['rect']['width'];
            let height = sceneAreaInfo['rect']['height'];
            let halfWidth = width / 2;
            let halfHeight = height / 2;
            
            sceneCollider.offset = cc.v2((left + halfWidth) * hroiFactor, -(top + halfHeight) * vertFactor);
            if (sceneAreaInfo['shape'] == 'rectangle') {
                sceneCollider.size = cc.size(width * hroiFactor, height * vertFactor);
            } else if (sceneAreaInfo['shape'] == 'circle') {
                sceneCollider.radius = halfWidth * hroiFactor;
            }
        }
    },

    _updateFreeTileContainer() {
        let rootNodeContainer = this.rootFreeTileNodeContainerNode();
        let freeTileNodeContainerName = this.generateFreeTileContainerNodeName();

        let freeTileNodeContainer = rootNodeContainer.getChildByName(freeTileNodeContainerName);
        if (freeTileNodeContainer == null) {
            return;
        }

        let dungeonNode = this.node;
        freeTileNodeContainer.setAnchorPoint(dungeonNode.getAnchorPoint());
        freeTileNodeContainer.scaleX = dungeonNode.scaleX;
        freeTileNodeContainer.scaleY = dungeonNode.scaleY;
        freeTileNodeContainer.scaleZ = dungeonNode.scaleZ;
        freeTileNodeContainer.angle = dungeonNode.angle;
        freeTileNodeContainer.setPosition(freeTileNodeContainer.parent.convertToNodeSpaceAR(dungeonNode.convertToWorldSpaceAR(cc.Vec2.ZERO)));
        freeTileNodeContainer.setContentSize(0, 0);
    },

    _applyColliderArea() {
        if (!this.dmInfo || !('collider_info' in this.dmInfo)) {
            return;
        }

        // 配置Rigidbody
        let rigidbody = this.node.getComponent(cc.RigidBody);
        if (!rigidbody) {
            rigidbody = this.node.addComponent(cc.RigidBody);
        }
        rigidbody.type = cc.RigidBodyType.Static;
        rigidbody.gravityScale = 0;
        rigidbody.fixedRotation = false;
        rigidbody.awakeOnLoad = true;

        // 配置所有独立碰撞区域
        this.colliderInfo = this.dmInfo['collider_info'];
        for (let index in this.colliderInfo) {
            let area = this.colliderInfo[index];

            // 添加PhysicsChainCollider组件
            let chainCollider = this.node.addComponent(cc.PhysicsChainCollider);
            chainCollider.tag = parseInt(index);

            // 添加碰撞点
            chainCollider.points = [];
            for (let point of area) {
                chainCollider.points.push(cc.v2(point.x, -point.y))
            }

            chainCollider.apply();
        }
    },

    _applySceneArea() {
        if (!this.dmInfo || !('scene_areas' in this.dmInfo)) {
            return;
        }

        // 配置所有独立碰撞区域
        let tagIndex = 0;
        this.sceneAreasInfo = this.dmInfo['scene_areas'];
        for (let sceneAreaName in this.sceneAreasInfo) {
            let areaInfo = this.sceneAreasInfo[sceneAreaName];

            let left = areaInfo['rect']['left'];
            let top = areaInfo['rect']['top'];
            let width = areaInfo['rect']['width'];
            let height = areaInfo['rect']['height'];
            let halfWidth = width / 2;
            let halfHeight = height / 2;
            
            if (areaInfo['shape'] == 'rectangle') {
                let collider = this.node.addComponent(cc.BoxCollider);
                collider.tag = tagIndex;
                collider.offset = cc.v2(left + halfWidth, -(top + halfHeight));
                collider.size = cc.size(width, height);
            } else if (areaInfo['shape'] == 'circle') {
                let collider = this.node.addComponent(cc.CircleCollider);
                collider.tag = tagIndex;
                collider.offset = cc.v2(left + halfWidth, -(top + halfHeight));
                collider.radius = halfWidth;
            }

            this.sceneAreas[tagIndex++] = sceneAreaName;
        }
    },

    _applyLayout() {
        this.dmInfo = {};
        this.colliderInfo = [];
        this.sceneAreasInfo = {};
        this.sceneAreas = {};

        this.dmInfo = this.dmAsset.getDmInfo();

        // 更新节点大小
        this.node.width = this.dmAsset.mapWidth;
        this.node.height = this.dmAsset.mapHeight;
        this.node.anchorX = 0;
        this.node.anchorY = 1;

        // 加载DungeonMapRenderLayer组件
        this.renderLayer = this.node.getComponent(dungeon.DungeonMapRenderLayer);
        if (!this.renderLayer) {
            this.renderLayer = this.node.addComponent(dungeon.DungeonMapRenderLayer);
        }

        // 初始化DungeonMapRenderLayer组件
        this.renderLayer.init(this, this.dmAsset);

        this._clearPhysicalComponent();
        this._clearColliderComponent();
    },

    _clearPhysicalComponent() {
        for (let collider of this.node.getComponents(cc.PhysicsChainCollider)) {
            collider.destroy();
        }
    },

    _clearColliderComponent() {
        for (let collider of this.node.getComponents(cc.Collider)) {
            collider.destroy();
        }
    },

    _clearLayout() {
        this._clearPhysicalComponent();
        this._clearColliderComponent();
        let rigidbody = this.node.getComponent(cc.RigidBody);
        if (rigidbody) {
            rigidbody.destroy();
        }
        this.renderLayer = this.node.getComponent(dungeon.DungeonMapRenderLayer);
        this.renderLayer.destroy();
        this.renderLayer = null;
        this.dmInfo = {};
        this.colliderInfo = [];
        this.sceneAreasInfo = {};
        this.sceneAreas = {};
    },

    convertToWorldPosition(row, col) {
        if (!this.dmAsset) {
            return cc.v2(0, 0);
        }

        if (row < 0 || col < 0 || row >= this.dmAsset.row || col >= this.dmAsset.col) {
            return cc.v2(0, 0);
        }

        let hroiFactor = this.node.width / this.dmAsset.mapWidth;
        let vertFactor = this.node.height / this.dmAsset.mapHeight;

        let tiledWidth = this.dmAsset.tiledWidth * hroiFactor;
        let tiledHeight = this.dmAsset.tiledHeight * vertFactor;

        let localPosition = cc.v2(col * tiledWidth, row * tiledHeight);
        localPosition.x += tiledWidth / 2;
        localPosition.y += tiledHeight / 2;
        localPosition.y = -localPosition.y;

        return this.node.convertToWorldSpaceAR(localPosition);
    },

    //
    // only for Editor
    //

    // 更新Free Tile的渲染图
    _updateFreeTileSpriteFrame: CC_EDITOR && function (freeTileNodeSpriteUuid, spriteFrameUuid) {
        Editor.Ipc.sendToPanel('scene', 'scene:set-property',{
            id: freeTileNodeSpriteUuid,
            path: "spriteFrame",
            type: "cc.SpriteFrame",
            value: {
                uuid: spriteFrameUuid
            },
            isSubProp: false,
        });
    },

    // 为Free Tile节点创建Animation组件
    _createAnimationComponentToFreeTileNode: CC_EDITOR && function (freeTileNode, animationName) {
        this.dmAsset.queryAnimationClipUuid(animationName, (uuid) => {
            let freeTileNodeAnimationComp = freeTileNode.addComponent(cc.Animation);
            freeTileNodeAnimationComp.playOnLoad = true;
            Editor.Ipc.sendToPanel('scene', 'scene:set-property',{
                id: freeTileNodeAnimationComp.uuid,
                path: "defaultClip",
                type: "cc.AnimationClip",
                value: {
                    uuid: uuid
                },
                isSubProp: false,
            });
        });
    },

    // 为Free Tile创建感应区Collider
    _createSceneAreaToFreeTileNode: CC_EDITOR && function (freeTileInfo, freeTileNode) {
        let polygonCollider = freeTileNode.addComponent(cc.PolygonCollider);
        polygonCollider.points = [];

        for (let colliderPoint of freeTileInfo['collider']) {
            polygonCollider.points.push(cc.v2(colliderPoint['x'], -colliderPoint['y']));
        }

        let sensor = freeTileNode.addComponent(dungeon.FreeTileSensor);
        sensor.opacityWhenMask = freeTileInfo['xopacity'] * 255;
    },

    // 为Free Tile创建物理碰撞体
    _createPhysicalColliderToFreeTileNode: CC_EDITOR && function (freeTileInfo, freeTileNode) {
        // 配置刚体
        let rigidbody = freeTileNode.addComponent(cc.RigidBody);
        rigidbody.type = cc.RigidBodyType.Static;
        rigidbody.fixedRotation = true;

        // 创建物理碰撞体
        let polygonPhysicalCollider = freeTileNode.addComponent(cc.PhysicsPolygonCollider);
        polygonPhysicalCollider.points = [];

        for (let phyColliderPoint of freeTileInfo['physical_collider']) {
            polygonPhysicalCollider.points.push(cc.v2(phyColliderPoint['x'], -phyColliderPoint['y']));
        }
        polygonPhysicalCollider.apply();
    },

    // 调整Free Tile节点布局
    _adjustFreeTileNodeLayout: CC_EDITOR && function (freeTileInfo, freeTileNode) {
        freeTileNode.anchorX = 0;
        freeTileNode.anchorY = 1;

        freeTileNode.width = freeTileInfo['width'];
        freeTileNode.height = freeTileInfo['height'];

        let freeTileNodePos = freeTileNode.parent.convertToNodeSpaceAR(this.node.convertToWorldSpaceAR(cc.v2(freeTileInfo['left'], -freeTileInfo['top'])));
        freeTileNode.setPosition(freeTileNodePos);
    },

    // 生成Free Tile节点
    _generateFreeTileNode: CC_EDITOR && function (freeTileInfo, freeTileNodeContainer) {
        
        //
        // 创建并配置节点
        //

        let freeTileNode = new cc.Node();
        if (freeTileInfo['nodename'].length != 0) {
            freeTileNode.name = freeTileInfo['nodename'];
        }
        if ('group_name' in freeTileInfo && freeTileInfo['group_name'].length != 0) {
            freeTileNode.group = freeTileInfo['group_name'];
        }
        if ('zindex' in freeTileInfo && freeTileInfo['zindex'] != 0) {
            freeTileNode.zIndex = freeTileInfo['zindex'];
        }
        freeTileNode.parent = freeTileNodeContainer;
        
        // 配置渲染
        let freeTileNodeSprite = freeTileNode.addComponent(cc.Sprite);
        freeTileNodeSprite.sizeMode = cc.Sprite.SizeMode.CUSTOM;
        freeTileNodeSprite.trim = false;
        if (freeTileInfo['type'] == 'picture') {
            this.dmAsset.queryPictureSpriteFrameUuid(freeTileInfo['info'], this._updateFreeTileSpriteFrame.bind(this, freeTileNodeSprite.uuid));
        } else if (freeTileInfo['type'] == 'animation') {
            let animationName = freeTileInfo['info']['animation']['animation_name'];

            // 装载第一帧
            this.dmAsset.queryAnimationFirstSpriteFrameUuid(animationName, this._updateFreeTileSpriteFrame.bind(this, freeTileNodeSprite.uuid));

            // 为当前free tile节点创建和配置animation组件
            this._createAnimationComponentToFreeTileNode(freeTileNode, animationName);
        }

        // 调整节点坐标与大小
        this._adjustFreeTileNodeLayout(freeTileInfo, freeTileNode);

        // 配置感应区
        if ('collider' in freeTileInfo && freeTileInfo['collider'].length != 0) {
            this._createSceneAreaToFreeTileNode(freeTileInfo, freeTileNode);
        }

        // 配置物理碰撞体
        if ('physical_collider' in freeTileInfo && freeTileInfo['physical_collider'].length != 0) {
            this._createPhysicalColliderToFreeTileNode(freeTileInfo, freeTileNode);
        }
    },

    // 生成所有Free Tile相关动画资源
    generateAnimationResources: CC_EDITOR && function () {
        if (this.dmAsset) {
            this.dmAsset.generateAnimationResources();
        }
    },

    // 删除所有Free Tile相关动画资源
    destroyAnimationResources: CC_EDITOR && function () {
        if (this.dmAsset) {
            this.dmAsset.removeAnimationAssetFolder();
        }
    },

    // 生成所有Free Tile节点
    generateFreeTileNodes: CC_EDITOR && function () {
        if (this.dmAsset == null) {
            return;
        }

        let dungeonNode = this.node;
        let rootNodeContainer = this.rootFreeTileNodeContainerNode();

        let freeTileNodeContainerName = this.generateFreeTileContainerNodeName();
        if (rootNodeContainer.getChildByName(freeTileNodeContainerName) != null) {
            cc.log("该地图已经生成过FreeTile节点，如果需要重新生成，请先删除旧节点");
            return;
        }

        let freeTileNodeContainer = new cc.Node();
        freeTileNodeContainer.name = freeTileNodeContainerName;
        freeTileNodeContainer.parent = rootNodeContainer;

        // 调整freeTileNodeContainer
        freeTileNodeContainer.setAnchorPoint(dungeonNode.getAnchorPoint());
        freeTileNodeContainer.scaleX = dungeonNode.scaleX;
        freeTileNodeContainer.scaleY = dungeonNode.scaleY;
        freeTileNodeContainer.scaleZ = dungeonNode.scaleZ;
        freeTileNodeContainer.angle = dungeonNode.angle;
        freeTileNodeContainer.setPosition(freeTileNodeContainer.parent.convertToNodeSpaceAR(dungeonNode.convertToWorldSpaceAR(cc.Vec2.ZERO)));
        freeTileNodeContainer.setContentSize(0, 0);

        // 生成所有free tile节点
        for (let freeTile of this.dmAsset.allFreeTileNodes()) {
            this._generateFreeTileNode(freeTile, freeTileNodeContainer);
        }
    },

    // 删除所有Free Tile节点
    destroyFreeTileNodes: CC_EDITOR && function () {
        let rootNodeContainer = this.rootFreeTileNodeContainerNode();
        let freeTileNodeContainerName = this.generateFreeTileContainerNodeName();

        let freeTileNodeContainer = rootNodeContainer.getChildByName(freeTileNodeContainerName);
        if (freeTileNodeContainer == null) {
            cc.log("未创建节点结构");
            return;
        }

        freeTileNodeContainer.destroyAllChildren();
        freeTileNodeContainer.destroy();
    }
});

dungeon.DungeonMap = DungeonMap;
module.exports = DungeonMap;