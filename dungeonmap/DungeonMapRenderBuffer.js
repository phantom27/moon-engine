// LICENSE HZQ
//
// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

let DungeonMapRenderBuffer = cc.Class({
    extends: cc.MeshBuffer
});

dungeon.DungeonMapRenderBuffer = DungeonMapRenderBuffer;
module.exports = DungeonMapRenderBuffer;