// LICENSE HZQ
//
// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

require('./DungeonMap')
require('./DungeonMapAsset')
require('./DungeonMapAssembler')

const RenderComponent = require('../cocos2d/core/components/CCRenderComponent');
const Material = require('../cocos2d/core/assets/material/CCMaterial');
const SpriteAtlas = require('../cocos2d/core/assets/CCSpriteAtlas');
const Texture2D = require('../cocos2d/core/assets/CCTexture2D');
const BlendFunc = require('../cocos2d/core/utils/blend-func');

let DungeonMapRenderLayer = cc.Class({
    name: 'dungeon.DungeonMapRenderLayer',
    extends: RenderComponent,
    mixins: [BlendFunc],
    editor: {
        requireComponent: dungeon.DungeonMap,
    },

    properties: {
        materials: {
            get () {
                return this._materials;
            },
            set (val) {
                this._materials = val;
                this._activateMaterial();
            },
            type: [Material],
            displayName: 'Materials',
            animatable: false,
            visible: false,
            override: true
        },
        dmAsset: {
            default: null,
            type: dungeon.DungeonMapAsset,
            visible: false
        },
        map: {
            default: null,
            type: dungeon.DungeonMap,
            visible: false
        }
    },
    
    ctor() {
        this.animationStatus = {};
        this.animationCount = 0;
    },

    __preload() {
        this._super();
    },

    update(dt) {
        if (this.animationCount) {
            this._updateAnimations();
        }
    },

    onEnable() {
        this._super();
        this._activateMaterial();
    },

    onDisable() {
        this._super();
    },

    dirty() {
        this.setVertsDirty();
    },

    _updateAnimations() {
        let dirty = false;
        let timestamp = Date.now();

        for (let index in this.animationStatus) {
            let animationTileInfo = this.animationStatus[index];
            if (timestamp - animationTileInfo['lastTimestamp'] >= animationTileInfo['animationRate']) {
                animationTileInfo['currentFrame'] = (animationTileInfo['currentFrame'] + 1) % animationTileInfo['totalFrames']
                animationTileInfo['lastTimestamp'] = timestamp;
                dirty = true;
            }
        }

        if (dirty) {
            this._updateRenderData(false, true);
        }
    },

    _updateRenderData(background = true, animation = true) {
        if (!this._renderData) {
            return;
        }

        if (background) {
            this._fillBackgroundRenderData();
        }
        if (animation) {
            this._fillAnimationTilesRenderData();
        }
        this._updateMaterial();
    },

    _initRenderData() {
        if (!this._assembler || !this.dmAsset) {
            return;
        }

        this._renderData = {};

        // 创建背景RenderData
        this._renderData['background'] = this._assembler.createRenderData();

        // 为所有存在动画的tiled创建RenderData
        this.animationStatus = {};
        this.animationCount = 0;
        if (this.dmAsset) {
            for (let info of this.dmAsset.allAnimationTileInfo()) {
                this._renderData[`${info['row']}#${info['col']}`] = this._assembler.createRenderData();
                this.animationStatus[`${info['row']}#${info['col']}`] = {
                    'row': parseInt(info['row']),
                    'col': parseInt(info['col']),
                    'animationName': info['animation_name'],
                    'frames': info['frames'],
                    'totalFrames': info['frames'].length,
                    'currentFrame': 0,
                    'animationRate': parseInt(info['animation_rate']),
                    'lastTimestamp': Date.now()
                };
                this.animationCount++;
            }
        }

        // 配置material
        this._activateMaterial();
    },

    _resetAssembler() {
        this._assembler = new dungeon.DungeonMapAssembler();
        this._assembler.init(this);
        this._initRenderData();
        this._updateColor();
        this.setVertsDirty();
    },

    _buildMaterials() {
        let matlen = 1 + this.animationCount;
        this._materials.length = matlen;

        for (let i = 0; i < matlen; i++) {
            if (!this._materials[i]) {
                this._materials[i] = Material.getBuiltinMaterial('2d-sprite');
            }
        }
    },

    _updateMaterial() {
        if (!this.dmAsset) {
            return;
        }

        // 配置background材质
        let material = this.getMaterial(0);
        material.setProperty('texture', this.dmAsset.backgroundTexture);

        // 配置所有动画的材质
        let materialIndex = 1;
        for (let index in this.animationStatus) {
            material = this.getMaterial(materialIndex++);

            let animationTileInfo = this.animationStatus[index];
            let frame = animationTileInfo['frames'][animationTileInfo['currentFrame']];

            let texture = frame['texture'];
            if (texture instanceof Texture2D) {
                material.setProperty('texture', texture);
            } else if (texture instanceof SpriteAtlas) {
                material.setProperty('texture', texture.getTexture());
            }
        }
    },

    _activateMaterial() {
        this._buildMaterials();
        this._super();
    },

    _validateRender() {

    },
    
    _fillBackgroundRenderData() {
        if (!('background' in this._renderData)) {
            return;
        }

        let backgroundRenderData = this._renderData['background'];
        let buffer = backgroundRenderData['buffer'];
        let inputAssembler = backgroundRenderData['inputAssembler'];

        if (!buffer || !inputAssembler) {
            return;
        }

        buffer.reset();
        buffer.request(4, 6);
        this._fillVertex(buffer, 0, 0, this.node.width, -this.node.height);
        this._fillIndices(buffer);
        this._fillUV(buffer, cc.v2(0, 0), cc.v2(1, 0), cc.v2(1, 1), cc.v2(0, 1));
        this._fillColor(buffer);
        inputAssembler._count = 6;
        buffer.uploadData();
    },

    _fillAnimationTileRenderData(tileIndex, animationTileInfo) {
        if (!(tileIndex in this._renderData)) {
            return;
        }

        let renderData = this._renderData[tileIndex];
        let buffer = renderData['buffer'];
        let inputAssembler = renderData['inputAssembler'];

        if (!buffer || !inputAssembler) {
            return;
        }

        // 提取当前帧
        let frame = animationTileInfo['frames'][animationTileInfo['currentFrame']];

        // 计算tile的位置
        let left = animationTileInfo['col'] * this.dmAsset.tiledWidth;
        let top = animationTileInfo['row'] * this.dmAsset.tiledHeight;

        // 提取uv信息
        let uv = frame['uv'];
        let uvLeft = uv['left'];
        let uvTop = uv['top'];
        let uvRight = uv['right'];
        let uvBottom = uv['bottom'];

        let hroiFactor = this.node.width / this.dmAsset.mapWidth;
        let vertFactor = this.node.height / this.dmAsset.mapHeight;

        buffer.reset();
        buffer.request(4, 6);
        this._fillVertex(buffer, left * hroiFactor, -top * vertFactor, this.dmAsset.tiledWidth * hroiFactor, -this.dmAsset.tiledHeight * vertFactor);
        this._fillIndices(buffer);
        this._fillUV(buffer, cc.v2(uvLeft, uvTop), cc.v2(uvRight, uvTop), cc.v2(uvRight, uvBottom), cc.v2(uvLeft, uvBottom));
        this._fillColor(buffer);
        inputAssembler._count = 6;
        buffer.uploadData();
    },

    _fillAnimationTilesRenderData() {
        for (let index in this.animationStatus) {
            this._fillAnimationTileRenderData(index, this.animationStatus[index]);
        }
    },

    _fillVertex(buffer, left, top, width, height) {
        let right = left + width;
        let bottom = top + height;

        let worldMatrix = cc.mat4();
        this.node._updateWorldMatrix();
        this.node.getWorldMatrix(worldMatrix);

        let leftTop = cc.v2(left, top).transformMat4(worldMatrix);
        let rightTop = cc.v2(right, top).transformMat4(worldMatrix);
        let rightBottom = cc.v2(right, bottom).transformMat4(worldMatrix);
        let leftBottom = cc.v2(left, bottom).transformMat4(worldMatrix);

        let vbuf = buffer._vData;

        // left top
        vbuf[0] = leftTop.x; vbuf[1] = leftTop.y;

        // right top
        vbuf[5] = rightTop.x; vbuf[6] = rightTop.y;

        // right bottom
        vbuf[10] = rightBottom.x; vbuf[11] = rightBottom.y;

        // right top
        vbuf[15] = leftBottom.x; vbuf[16] = leftBottom.y;
    },

    _fillIndices(buffer) {
        let ibuf = buffer._iData;

        // right top triangle
        ibuf[0] = 0; ibuf[1] = 1; ibuf[2] = 2;

        // left bottom triangle
        ibuf[3] = 0; ibuf[4] = 2; ibuf[5] = 3;
    },

    _fillUV(buffer, leftTop, rightTop, rightBottom, leftBottom) {
        let vbuf = buffer._vData;

        // left top
        vbuf[2] = leftTop.x; vbuf[3] = leftTop.y;

        // right top
        vbuf[7] = rightTop.x; vbuf[8] = rightTop.y;

        // right bottom
        vbuf[12] = rightBottom.x; vbuf[13] = rightBottom.y;

        // left bottom
        vbuf[17] = leftBottom.x; vbuf[18] = leftBottom.y;
    },

    _fillColor(buffer) {
        let uibuf = buffer._uintVData;
        
        // left top
        uibuf[4] = this.node.color._val;

        // right top
        uibuf[9] = this.node.color._val;

        // right bottom
        uibuf[14] = this.node.color._val;

        // left bottom
        uibuf[19] = this.node.color._val;
    },

    init(map, asset) {
        this.map = map;
        this.dmAsset = asset;

        if (!this._assembler) {
            this._assembler = new dungeon.DungeonMapAssembler();
            this._assembler.init(this);
        }
        
        this._initRenderData();
        this._updateColor();
        this.setVertsDirty();
    },
});

dungeon.DungeonMapRenderLayer = DungeonMapRenderLayer;
module.exports = DungeonMapRenderLayer;