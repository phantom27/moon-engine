// LICENSE HZQ
//
// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

require('./DungeonMapRenderBuffer');

import Assembler from '../cocos2d/core/renderer/assembler';

const DungeonMapTileVertexFormat = new cc.gfx.VertexFormat([
    { name: cc.gfx.ATTR_POSITION, type: cc.gfx.ATTR_TYPE_FLOAT32, num: 2 },
    { name: cc.gfx.ATTR_UV0, type: cc.gfx.ATTR_TYPE_FLOAT32, num: 2 },
    { name: cc.gfx.ATTR_COLOR, type: cc.gfx.ATTR_TYPE_UINT8, num: 4, normalize: true },
]);

export default class DungeonMapAssembler extends Assembler {
    
    init(renderComponent) {
        super.init(renderComponent);
    }

    createRenderData() {
        let buffer = new dungeon.DungeonMapRenderBuffer(cc.renderer._handle, this.getVfmt());
        let inputAssembler = new cc.renderer.InputAssembler();
        inputAssembler._vertexBuffer = buffer._vb;
        inputAssembler._indexBuffer = buffer._ib;
        inputAssembler._start = 0;
        inputAssembler._count = 0;
        return {
            'buffer': buffer,
            'inputAssembler': inputAssembler
        }
    }

    updateRenderData(renderComponent) {
        renderComponent._updateRenderData();
    }

    fillBuffers(renderComponent, renderer) {
        if (!renderComponent._renderData) {
            return;
        }

        renderer.node = renderComponent.node;

        let materialIndex = 0;
        for (let index in renderComponent._renderData) {
            let renderData = renderComponent._renderData[index];
            renderer.material = renderComponent.getMaterial(materialIndex++);
            renderer._flushIA(renderData['inputAssembler']);
        }
    }

    updateColor(renderComponent, color) {

    }

    getVfmt() {
        return DungeonMapTileVertexFormat;
    }
}

dungeon.DungeonMapAssembler = DungeonMapAssembler;