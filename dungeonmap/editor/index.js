const DungeonMapMeta = require('./DungeonMapMeta')

if (CC_EDITOR && Editor !== undefined) {
    if (Editor.metas !== undefined) {
        Editor.metas['dungeon-map'] = DungeonMapMeta
        Editor.metas['dungeon-map']['asset-type'] = 'dungeon-map';
        Editor.metas['dungeon-map']['asset-icon'] = "unpack://engine/dungeonmap/editor/dungeon-map.png";
    }

    if (Editor.inspectors !== undefined) {
        Editor.inspectors['dungeon-map'] = 'packages://inspector/inspectors/text.js';
    }

    if (Editor.assets !== undefined) {
        Editor.assets['dungeon-map'] = dungeon.DungeonMapAsset;
    }

    if (Editor.assettype2name !== undefined) {
        Editor.assettype2name['dungeon.DungeonMapAsset'] = 'dungeon-map'
    }

    if (Editor.isMainProcess && Editor.assetdb !== undefined) {
        Editor.assetdb.register('.dm', false, DungeonMapMeta);
    }
}