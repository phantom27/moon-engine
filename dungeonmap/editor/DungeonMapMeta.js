'use strict';

const Fs = require('fire-fs')
const Path = require('fire-path')
const GZip = require('../../cocos2d/compression/gzip')

const DmEncoding = { encoding: 'binary' };

const DmAssetMember = [
    'magic',
    'version',
    'dungeon_name',
    'background',
    'tiled_row',
    'tiled_col',
    'tiled_width',
    'tiled_height',
    'width',
    'height',
    'total_tiles',
    'tiles',
    'rooms',
    'resources',
    'animations',
    'tile_sprite_infos',
    'tile_collider_flags',
    'collider_info',
    'tile_extra_infos',
    'scene_areas',
    'depend_resource',
    'depend_sprite_info'
];

const DmAssetNumberMember = [
    'tiled_row',
    'tiled_col',
    'tiled_width',
    'tiled_height',
    'width',
    'height',
    'total_tiles'
];

function verifyDungeonMapAssetValid(dm) {
    if (!dm) {
        return false;
    }

    for (var member of DmAssetMember) {
        if (!dm.hasOwnProperty(member)) {
            return false;
        }
    }

    if (dm['magic'] != 'dmd') {
        return false;
    }

    for (var numberMember of DmAssetNumberMember) {
        if (typeof(dm[numberMember]) !== 'number' || dm[numberMember] <= 0) {
            return false;
        }
    }

    for (var notEmptyMember of ['dungeon_name', 'background']) {
        if (dm[notEmptyMember] === '') {
            return false;
        }
    }

    return true;
}

async function verifyDungeonMapAssetDependResources(dmFilePath, dmInfo, callback) {

    let info = {};
    info.dmDir = Path.dirname(dmFilePath);
    info.dmResourceDir = Path.join(info.dmDir, 'resources')

    // 检查背景图是否存在
    info.backgroundPath = Path.join(info.dmDir, dmInfo['background'])
    if (!Fs.existsSync(info.backgroundPath)) {
        return callback(new Error("background not exists"), null);
    }

    // 检查所有依赖资源是否存在
    info.imagesResource = [];
    info.spritesResource = [];
    for (let resource of dmInfo['depend_resource']) {
        let resource_path = resource.replace("/", Path.sep);
        let splite = resource_path.split(Path.sep);
        if (splite.length <= 0) {
            continue;
        }
        let type = splite[0];
        resource_path = Path.join(info.dmResourceDir, resource_path)

        if (!Fs.existsSync(resource_path)) {
            return callback(new Error(`resource "${resource}" not exists`), null);
        }

        info[`${type}Resource`].push(resource_path)
    }

    callback(null, info)
}

class DungeonMapMeta extends Editor.metas['custom-asset'] {
    constructor(assetdb) {
        super(assetdb);
        this._dmRawDataBinary = null;
        this._dmRawData = '';
        this._dmInfo = {};
    }

    static version() { return '0.1.0'; }
    static defaultType() { return 'dungeon-map'; }

    static validate(assetPath) {
        return true
    }

    import(fspath, callback) {
        Fs.readFile(fspath, DmEncoding, (err, data) => {
            if (err) {
                return callback(err);
            }

            this._dmRawDataBinary = data;
            this._dmRawData = GZip.gunzip(this._dmRawDataBinary);

            try {
                this._dmInfo = JSON.parse(this._dmRawData);
            } catch (e) {
                return callback(e)
            }

            // 检查dm数据是否有效
            if (!verifyDungeonMapAssetValid(this._dmInfo)) {
                return callback(new Error('dungeon map asset is invalid'));
            }

            // 检查依赖资源是否存在
            verifyDungeonMapAssetDependResources(fspath, this._dmInfo, (e, info) => {
                if (e) {
                    return callback(e);
                }
                
                this._dmInfo.dmDir = info.dmDir;
                this._dmInfo.dmResourceDir = info.dmResourceDir;
                this._dmInfo.backgroundPath = info.backgroundPath;
                this._dmInfo.imagesResource = info.imagesResource;
                this._dmInfo.spritesResource = info.spritesResource;

                callback();
            })
        })
    }

    postImport(fspath, callback) {
        let asset = new dungeon.DungeonMapAsset()
        asset.name = Path.basenameNoExt(fspath)
        asset.dmRawDataBinary = this._dmRawDataBinary;
        asset.row = this._dmInfo['tiled_row'];
        asset.col = this._dmInfo['tiled_col'];
        asset.tiledWidth = this._dmInfo['tiled_width'];
        asset.tiledHeight = this._dmInfo['tiled_height'];
        asset.mapWidth = this._dmInfo['width'];
        asset.mapHeight = this._dmInfo['height'];

        // 加载background
        let backgroundUuid = this._assetdb.fspathToUuid(this._dmInfo.backgroundPath);
        if (!backgroundUuid) {
            return callback(new Error('loading background failed'));
        }
        asset.backgroundTexture = Editor.serialize.asAsset(backgroundUuid);

        // 加载依赖的资源
        let db = this._assetdb;
        for (let imageIndex in this._dmInfo.imagesResource) {
            let p = this._dmInfo.imagesResource[imageIndex];
            let basename = Path.basename(p);
            var imageUuid = db.fspathToUuid(p);
            asset.imageTextures.push(imageUuid ? Editor.serialize.asAsset(imageUuid) : null);
            asset.imageTextureIndex[basename] = parseInt(imageIndex);
        }
        for (let spriteIndex in this._dmInfo.spritesResource) {
            let p = this._dmInfo.spritesResource[spriteIndex];
            let basename = Path.basename(p);
            var spriteUuid = db.fspathToUuid(p);
            asset.spriteTextures.push(spriteUuid ? Editor.serialize.asAsset(spriteUuid) : null);
            asset.spriteTextureIndex[basename] = parseInt(spriteIndex);
        }

        this._assetdb.saveAssetToLibrary(this.uuid, asset);
        callback()
    }
}

dungeon.DungeonMapMeta = DungeonMapMeta;
module.exports = DungeonMapMeta;
