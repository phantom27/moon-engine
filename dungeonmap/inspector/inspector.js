Vue.component('dungeon-map-inspector', {
    data: function() {
        return {

        }
    },

    template: `
        <ui-prop v-prop="target.dmAsset"></ui-prop>
        <ui-prop v-prop="target.freeTileContainer"></ui-prop>
        <cc-array-prop :target.sync="target.sceneColliderHandlers"></cc-array-prop>
        <br/>
        <center>
            <span>
                <ui-button class="green" @confirm="generateAnimationResources">生成Free Tile相关动画资源</ui-button>
                <ui-button class="red" @confirm="destroyAnimationResources">删除Free Tile相关动画资源</ui-button>
                <ui-button class="green" @confirm="generateFreeTileNodes">生成所有Free Tile节点</ui-button>
                <ui-button class="red" @confirm="destroyFreeTileNodes">删除所有Free Tile节点</ui-button>
            </span>
        </center>
    `,

    props: {
        target: {
            twoWay: true,
            type: Object,
        },
    },

    methods: {
        getTargetComponent() {
            return cc.engine.getInstanceById(this.target["node"].value.uuid).getComponents(cc.Component).find(comp => comp.uuid == this.target["uuid"].value);
        },
        generateAnimationResources() {
            this.getTargetComponent().generateAnimationResources();
            Editor.Ipc.sendToPanel('scene', 'scene:undo-commit');
        },
        destroyAnimationResources() {
            this.getTargetComponent().destroyAnimationResources();
            Editor.Ipc.sendToPanel('scene', 'scene:undo-commit');
        },
        generateFreeTileNodes() {
            this.getTargetComponent().generateFreeTileNodes();
            Editor.Ipc.sendToPanel('scene', 'scene:undo-commit');
        },
        destroyFreeTileNodes() {
            this.getTargetComponent().destroyFreeTileNodes();
            Editor.Ipc.sendToPanel('scene', 'scene:undo-commit');
        },
    }
});