declare namespace dungeon {
    export class DungeonMapAsset extends cc.Asset {
        dmRawDataBinary: Uint8Array;
        dmInfo: Object;
        row: number;
        col: number;
        tiledWidth: number;
        tiledHeight: number;
        mapWidth: number;
        mapHeight: number;
        backgroundTextre: cc.Texture2D;
        imageTextures: [cc.Texture2D];
        imageTextureIndex: Object;
        spriteTextures: [cc.Texture2D];
        spriteTextureIndex: Object;

        getDmInfo(): Object;
        allSpriteListInfo(): Object;
        allAnimationTileInfo(): [Object];
        allFreeTileNodes(): [Object];
    }

    export class DungeonMap extends cc.Component {
        dmAsset: DungeonMapAsset;
        sceneColliderHandlers: [cc.Component.EventHandler];
        freeTileContainer: cc.Node;

        dungeonName(): string;
        rootFreeTileNodeContainerNode(): cc.Node;
        generateFreeTileContainerNodeName(): string;
        mapWidth(): number;
        mapHeight(): number;
        tiledWidth(): number;
        tiledHeight(): number;
        row(): number;
        col(): number;
        tiles(): [any];
        tileExtraInfos(): [any];
        indexToPosition(index): cc.Vec2;
        startPoints(): [cc.Vec2];
        convertToWorldPosition(row, col): cc.Vec2;
    }
}