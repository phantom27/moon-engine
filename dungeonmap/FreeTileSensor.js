// LICENSE HZQ
//
// Learn cc.Class:
//  - https://docs.cocos.com/creator/manual/en/scripting/class.html
// Learn Attribute:
//  - https://docs.cocos.com/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - https://docs.cocos.com/creator/manual/en/scripting/life-cycle-callbacks.html

let FreeTileSensor = cc.Class({
    name: 'dungeon.FreeTileSensor',
    extends: cc.Component,

    properties: {
        _enterCounter: 0,
        _opacityWhenMask: {
            default: 200,
            serializable: true
        },
        opacityWhenMask: {
            get () {
                return this._opacityWhenMask;
            },
            set (val) {
                this._opacityWhenMask = val;
            },
            displayName: 'Opacity When Mask',
        },
    },
    
    onCollisionEnter: function (other, self) {
        this._enterCounter++;
        if (this._enterCounter == 1) {
            this.node.opacity = this.opacityWhenMask;
        }
    },

    onCollisionExit: function (other, self) {
        this._enterCounter--;
        if (this._enterCounter == 0) {
            this.node.opacity = 255;
        }
    }
});

dungeon.FreeTileSensor = FreeTileSensor;
module.exports = FreeTileSensor;